import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/homescreen';
import UsersScreen from './src/userScreen';
import Gesto from './src/gestosScreen';

const Stack = createStackNavigator();

export default function App() {
  return (
   <NavigationContainer>
    <Stack.Navigator initialRouteName='Home'>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Users" component={UsersScreen} />
        <Stack.Screen name="Gestos" component={Gesto} />
      </Stack.Navigator>
   </NavigationContainer>
  );
}


 

